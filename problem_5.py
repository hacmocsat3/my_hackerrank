from itertools import groupby


def solve(st):
    """
    A newly opened multinational brand has decided to base
    their company logo on the three most common characters in the company name.
    They are now trying out various combinations of company names and logos
    based on this condition.Given a string, which is the company name
    in lowercase letters,your task is to find the top three most common
    characters in the string.

    link problem: https://hackerrank.com/challenges/most-commons/problem

    """

    data = sorted(list(st))
    key = []
    values = []

    for k, v in groupby(data):
        key.append(k)
        values.append(len(list(v)))

    result = sorted(list(zip(key, values)),
                    key=lambda x: x[1], reverse=True)[:3]

    for item in result:
        print(item[0], item[1])


def main():
    st = input()
    solve(st)


if __name__ == "__main__":
    main()
