def solve(n):
    """
    You are given n words. Some words may repeat.For each word,
    output its number of occurrences.The output order should correspond
    with the input order of appearance of the word.See the sample
    input/output for clarification.

    Note: Each input line ends with a "\n" character.


    link problem: https://www.hackerrank.com/challenges/word-order/problem
    """

    dic = {}
    for _ in range(n):
        word = input()
        if word in dic:
            dic[word] += 1
        else:
            dic[word] = 1
    print(len(dic))
    for k in dic:
        print(dic[k], end=" ")


def main():

    n = int(input())
    solve(n)


if __name__ == "__main__":
    main()
