"""
There is an array of n integers. There are also 2 disjoint sets,
A and B, each containing m integers.
You like all the integers in set A and dislike all the integers in set B .
Your initial happiness is 0.
For each i integer in the array, if i E A, you add 1 to your happiness.
If i E B , you add -1 to your happiness.
Otherwise, your happiness does not change.
Output your final happiness at the end.

Note: Since  and  are sets, they have no repeated elements.
      However, the array might contain duplicate elements.

Input Format:
    The first line contains integers n and m separated by a space.
    The second line contains n integers, the elements of the array.
    The third and fourth lines contain m integers, A and B, respectively.

Output Format:
    Output a single integer, your total happiness.

link problem: https://www.hackerrank.com/challenges/no-idea/problem
"""


def solve():
    n, m = map(int, input().split())
    N = list(map(int, input().split()))
    A = set(map(int, input().split()))
    B = set(map(int, input().split()))

    total = 0
    C = A.union(B)
    N = [i for i in N if i in C]

    for i in N:
        if i in A:
            total += 1
        else:
            total -= 1
    return total


def main():
    print(solve())


if __name__ == "__main__":
    main()
